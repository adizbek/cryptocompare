import {OPEN, Server} from 'ws';
import {getRepository} from 'typeorm';
import {Compare} from '../core/entity/Compare';

const broadcastInterval = Math.max(parseInt(process.env.RANDOM_CURRECY_BROADCAST_INTERVAL) || 5000, 5000);
const WsPort = parseInt(process.env.WS_PORT) || 8080;

const wss = new Server({port: WsPort, host: '0.0.0.0'});

wss.on('connection', function connection() {
	console.log('New socket connected');
});

const CompareRepository = getRepository(Compare);

async function broadcastRandomCurrency() {
	try {
		const compare: Compare = (await CompareRepository.createQueryBuilder()
			.select('*')
			.orderBy('RAND()')
			.limit(1)
			.execute());

		for (const client of wss.clients.values()) {
			if (client.readyState === OPEN) {
				client.send(JSON.stringify({event: 'randomCurrency', payload: [compare]}));
			}
		}
	} catch (e) {
		console.log('Broadcasting failed', e);
	}
}

const Broadcaster = setInterval(broadcastRandomCurrency, broadcastInterval);

function gracefulShutdown() {
	clearInterval(Broadcaster);
	console.log('Broadcaster stopped');
}

process.on('SIGINT', gracefulShutdown);
process.on('SIGTERM', gracefulShutdown);
