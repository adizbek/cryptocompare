import Fastify from 'fastify';
import CryptoCompareService from '../core/services/CryptoCompareService';
import CryptoCompareLocalProvider from "../core/services/CryptoCompareLocalProvider";
import CryptoCompareExternalProvider from "../core/services/CryptoCompareExternalProvider";

const fastify = Fastify({
	logger: true,
});

const cryptoCompareService = new CryptoCompareService(CryptoCompareExternalProvider, CryptoCompareLocalProvider);

fastify.get('/service/price', async (request, reply) => {
	try {
		const {fsyms: from, tsyms: to} = request.query as { fsyms: string, tsyms: string };

		if (!from || !to) {
			reply.status(400).send({
				success: false,
				message: 'Bad request, invalid currencies'
			});
		}

		const prices = await cryptoCompareService.getPrices(from, to);

		reply.send({
			success: true,
			data: prices
		});
	} catch (e) {
		reply.status(400).send({
			success: false,
			message: 'Bad request'
		});
	}
});

const PORT = parseInt(process.env.APP_PORT) || 3000;

fastify.listen(PORT, '0.0.0.0', (err, address) => {
	if (err) throw err;

	console.log(`Server started successfully at ${address}`);
});

async function gracefulShutdown() {
	try {
		await fastify.close();
		console.log('Fastify stopped');
	} catch (e) {
		console.log('Error while stopping server', e);
	}
}

process.on('SIGINT', gracefulShutdown);
process.on('SIGTERM', gracefulShutdown);
