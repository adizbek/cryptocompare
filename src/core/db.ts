import {Connection, createConnection} from 'typeorm';

export async function connectToDb(): Promise<Connection> {
	const connection = await createConnection();

	console.log('Connected to database');

	return connection;
}
