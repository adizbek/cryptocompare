import {Column, CreateDateColumn, Entity, Index, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
@Index(['direction'], {unique: true})
@Index(['from', 'to'], {unique: true})
export class Compare {

	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	from: string;

	@Column()
	to: string;

	@Column()
	direction: string;

	@CreateDateColumn()
	createdAt: Date;

	@Column({
		type: 'json',
	})
	details: Record<string, unknown>;

	@Column({
		type: 'json',
	})
	display: Record<string, unknown>;

	constructor(from: string, to: string, details: Record<string, unknown>, display: Record<string, unknown>) {
		this.from = from;
		this.to = to;
		this.details = details;
		this.display = display;
		this.direction = `${from}-${to}`;
	}
}
