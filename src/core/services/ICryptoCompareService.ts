import {CryptoCompareResult} from "./ICryptoCompareProvider";

export default interface ICryptoCompareService {
	getPrices(from: string, to: string): Promise<CryptoCompareResult>;
}
