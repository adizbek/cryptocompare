import ICryptoCompareProvider, {CryptoCompareResult, ICryptoComparePersistentProvider} from './ICryptoCompareProvider';
import {getRepository, In, Repository} from 'typeorm';
import {Compare} from '../entity/Compare';

class CryptoCompareLocalProvider implements ICryptoCompareProvider, ICryptoComparePersistentProvider {
	private compareRepository: Repository<Compare>;

	constructor(compareRepository: Repository<Compare>) {
		this.compareRepository = compareRepository;
	}

	async fetchData(from: string, to: string): Promise<CryptoCompareResult> {
		const directions = CryptoCompareLocalProvider.getDirections(from, to);

		const compareResults = await this.compareRepository.find({
			where: {
				direction: In(directions)
			}
		});

		return CryptoCompareLocalProvider.convertToResult(compareResults);
	}

	private static getDirections(from: string, to: string): string[] {
		const fromArr = from.split(',');
		const toArr = to.split(',');
		const directions = [];

		for (const a of fromArr) {
			for (const b of toArr) {
				directions.push(`${a}-${b}`);
			}
		}

		return directions;
	}

	private static convertToResult(data: Compare[]): CryptoCompareResult {
		const result: CryptoCompareResult = {
			RAW: {},
			DISPLAY: {}
		};

		for (const compareData of data) {
			if (!result.RAW[compareData.from]) {
				result.RAW[compareData.from] = {};
				result.RAW[compareData.from][compareData.to] = compareData.details;
			}

			if (!result.DISPLAY[compareData.from]) {
				result.DISPLAY[compareData.from] = {};
				result.DISPLAY[compareData.from][compareData.to] = compareData.display;
			}
		}

		return result;
	}

	async savePrices(prices: CryptoCompareResult): Promise<Compare[]> {
		const fromCurrencies = Object.keys(prices.RAW);

		const newData: {
			from: string
			to: string
			direction: string
			details: Record<string, unknown>
			display: Record<string, unknown>
		}[] = [];

		for (const fromCurrency of fromCurrencies) {
			const toCurrencies = Object.keys(prices.RAW[fromCurrency]);

			for (const toCurrency of toCurrencies) {
				newData.push({
					from: fromCurrency,
					to: toCurrency,
					direction: `${fromCurrency}-${toCurrency}`,
					details: prices.RAW[fromCurrency][toCurrency],
					display: prices.DISPLAY[fromCurrency][toCurrency]
				});
			}
		}

		const currentData = (await this.compareRepository.find({
			where: {
				direction: In(newData.map(d => d.direction))
			}
		})).reduce((map, item) => {
			map.set(item.direction, item);

			return map;
		}, new Map<string, Compare>());

		const dataToInsert = newData.filter(d => !currentData.has(d.direction))
			.map((item) => new Compare(item.from, item.to, item.details, item.display));

		const dataToUpdate = newData.filter(d => currentData.has(d.direction))
			.map(newData => {
				const oldData = currentData.get(newData.direction);

				oldData.display = newData.display;
				oldData.details = newData.details;

				return oldData;
			});

		const dataToSave = [...dataToInsert, ...dataToUpdate];

		await this.compareRepository.save(dataToSave);

		return dataToSave;
	}

}

export default new CryptoCompareLocalProvider(getRepository(Compare));
