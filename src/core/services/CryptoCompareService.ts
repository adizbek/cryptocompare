import ICryptoCompareProvider, {CryptoCompareResult} from './ICryptoCompareProvider';
import ICryptoCompareService from "./ICryptoCompareService";

export default class CryptoCompareService implements ICryptoCompareService {
	private externalProvider: ICryptoCompareProvider;
	private localProvider: ICryptoCompareProvider;

	constructor(externalProvider: ICryptoCompareProvider, localProvider: ICryptoCompareProvider) {
		this.externalProvider = externalProvider;
		this.localProvider = localProvider;
	}

	async getPrices(from: string, to: string): Promise<CryptoCompareResult> {
		try {
			// First lookup inside local provider
			const localResults = await this.localProvider.fetchData(from, to);

			// comparison data not found search with API
			if (localResults && Object.keys(localResults.RAW).length > 0) {
				return localResults;
			}

			return await this.externalProvider.fetchData(from, to);
		} catch (e) {
			// if there was some error, just return empty result
			return {
				RAW: {},
				DISPLAY: {}
			};
		}
	}

}
