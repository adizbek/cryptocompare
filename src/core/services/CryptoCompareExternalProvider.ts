import axios from 'axios';
import ICryptoCompareProvider, {CryptoCompareResult} from './ICryptoCompareProvider';

export const httpClient = axios.create({
	baseURL: 'https://min-api.cryptocompare.com/'
});

class CryptoCompareExternalProvider implements ICryptoCompareProvider {
	async fetchData(from: string, to: string): Promise<CryptoCompareResult> {
		const response = await httpClient.get('/data/pricemultifull', {
			params: {
				fsyms: from,
				tsyms: to
			},

			timeout: 5000
		});

		return response.data as CryptoCompareResult;
	}
}

export default new CryptoCompareExternalProvider();
