import {Compare} from "../entity/Compare";

export interface CryptoCompareResult {
	RAW: {
		[key: string]: { [key: string]: Record<string, unknown> }
	},
	DISPLAY: {
		[key: string]: { [key: string]: Record<string, unknown> }
	}
}

export interface ICryptoComparePersistentProvider {
	savePrices(prices: CryptoCompareResult): Promise<Compare[]>
}

export default interface ICryptoCompareProvider {
	fetchData(from: string, to: string): Promise<CryptoCompareResult>;
}
