import ICryptoCompareProvider, {ICryptoComparePersistentProvider} from "../core/services/ICryptoCompareProvider";

export default class ScheduledUpdater {

	interval: NodeJS.Timer;

	static readonly MinPeriod = 60000
	private readonly period: number;
	private readonly from: string;
	private readonly to: string;
	private externalProvider: ICryptoCompareProvider;
	private persistentProvider: ICryptoComparePersistentProvider;

	constructor(period: number, from: string, to: string, externalProvider: ICryptoCompareProvider, persistentProvider: ICryptoComparePersistentProvider) {
		this.externalProvider = externalProvider;
		this.persistentProvider = persistentProvider;

		this.from = from;
		this.to = to;
		this.period = Math.max(Number.isSafeInteger(period) ? period : ScheduledUpdater.MinPeriod, ScheduledUpdater.MinPeriod);
		this.interval = setInterval(() => this.scheduledUpdate(), this.period)
	}

	async scheduledUpdate() {
		try {
			console.log('Fetching updates...');

			const prices = await this.externalProvider.fetchData(this.from, this.to);

			const compares = await this.persistentProvider.savePrices(prices);

			console.log(`${compares.length} compares saved to database`);
		} catch (e) {
			console.log('Error while fetching updates', e);
		}
	}

	destroy() {
		clearInterval(this.period)
	}

}
