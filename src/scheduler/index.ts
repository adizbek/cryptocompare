import ScheduledUpdater from "./ScheduledUpdater";
import CryptoCompareExternalProvider from "../core/services/CryptoCompareExternalProvider";
import CryptoCompareLocalProvider from "../core/services/CryptoCompareLocalProvider";

console.log('Scheduler service has started');

const from = process.env.FSYMS;
const to = process.env.TSYMS;

const CurrencyRatesUpdateInterval = parseInt(process.env.SCHEDULER_INTERVAL);
const ExternalProvider = CryptoCompareExternalProvider;
const PersistentProvider = CryptoCompareLocalProvider;

const Updater = new ScheduledUpdater(CurrencyRatesUpdateInterval, from, to, ExternalProvider, PersistentProvider)

function gracefulShutDown() {
	Updater.destroy()
	console.log('Scheduler stopped');
}

process.on('SIGINT', gracefulShutDown);
process.on('SIGTERM', gracefulShutDown);
