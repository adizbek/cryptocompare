// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

import {Connection} from 'typeorm';
import 'reflect-metadata';
import {connectToDb} from './core/db';

const app = process.env.APP ?? 'app';
let db: Connection;

async function start() {
	db = await connectToDb();

	if (app === 'scheduler') {
		require('./scheduler');
	} else if (app === 'broadcaster') {
		require('./broadcaster');
	} else {
		require('./web');
	}
}

start().then(() => {
	console.log(`${app} started 🚀`);
}).catch((err) => {
	console.log('Start failed!', err);
	process.exit(1);
});

async function gracefulShutdown() {
	try {
		if (db) {
			await db.close();
			console.log('Database closed');
		}
	} catch (e) {
		console.log('Error while shutdown', e);
	}
}

process.on('SIGINT', gracefulShutdown);
process.on('SIGTERM', gracefulShutdown);

process.on('uncaughtException', (err) => {
	console.log('Exception was not caught', err);

	process.exit(1);
});

process.on('unhandledRejection', (err) => {
	console.log('Exception was not caught', err);

	process.exit(1);
});
