require('chai').should()

import ScheduledUpdater from "../../../src/scheduler/ScheduledUpdater";
import ICryptoCompareProvider, {
	CryptoCompareResult,
	ICryptoComparePersistentProvider
} from "../../../src/core/services/ICryptoCompareProvider";
import {Compare} from "../../../src/core/entity/Compare";
import sinon from "sinon";

class FakeCryptoCompareProvider implements ICryptoCompareProvider {
	fetchData(from: string, to: string): Promise<CryptoCompareResult> {
		return Promise.resolve({
			RAW: {}, DISPLAY: {}
		});
	}
}

class FakeLocalPersistentProvider implements ICryptoComparePersistentProvider {
	savePrices(prices: CryptoCompareResult): Promise<Compare[]> {
		return Promise.resolve([]);
	}
}

describe('Test ScheduledUpdater', function () {
	it('should scheduledUpdate correctly', function () {
		const fakeExternal = new FakeCryptoCompareProvider();
		const fakeLocalPersistent = new FakeLocalPersistentProvider();

		let timer = sinon.useFakeTimers();
		let period = 60000;
		let updater = new ScheduledUpdater(period, 'USD', 'BTC', fakeExternal, fakeLocalPersistent);
		let spy = sinon.spy(updater);

		timer.tick((period+1) * 2 )
		spy.scheduledUpdate.calledTwice.should.be.true

		updater.destroy()
	});
});
