import ICryptoCompareProvider, {CryptoCompareResult} from "../../../src/core/services/ICryptoCompareProvider";
import CryptoCompareService from "../../../src/core/services/CryptoCompareService";
import sinon from "sinon";

require('chai').should()

class FakeProviderWithValue implements ICryptoCompareProvider {
	fetchData(from: string, to: string): Promise<CryptoCompareResult> {
		return Promise.resolve({
			RAW: {
				BTC: {
					USD: {}
				}
			},
			DISPLAY: {
				BTC: {
					USD: {}
				}
			}
		});
	}
}

class FakeProviderEmpty implements ICryptoCompareProvider {
	fetchData(from: string, to: string): Promise<CryptoCompareResult> {
		return Promise.resolve({
			RAW: {},
			DISPLAY: {}
		});
	}
}

class FakeProviderThrows implements ICryptoCompareProvider {
	fetchData(from: string, to: string): Promise<CryptoCompareResult> {
		return Promise.reject('Failed to load');
	}
}

function spyService(external: ICryptoCompareProvider, local: ICryptoCompareProvider) {
	return {
		local,
		external,
		spyLocal: sinon.spy(local),
		spyExternal: sinon.spy(external),
		service: new CryptoCompareService(external, local)
	}
}

describe('Test CryptoCompareService', function () {
	it('should load from local provider if all key are available in local database', async function () {
		let spy = spyService(new FakeProviderEmpty(), new FakeProviderWithValue())

		let results = await spy.service.getPrices('BTC', 'USD');

		results.RAW.should.have.key('BTC')
		results.RAW.BTC.should.have.key('USD')

		spy.spyExternal.fetchData.notCalled.should.be.true
		spy.spyLocal.fetchData.calledOnce.should.be.true
	});

	it('should load from external provider if prices not available in local', async function () {
		let spy = spyService(new FakeProviderWithValue(), new FakeProviderEmpty())

		let results = await spy.service.getPrices('BTC', 'USD');

		results.RAW.should.have.key('BTC')
		results.RAW.BTC.should.have.key('USD')

		spy.spyExternal.fetchData.calledOnce.should.be.true

		spy.spyLocal.fetchData.calledOnce.should.be.true

		let localResult = await spy.spyLocal.fetchData.returnValues[0];
		localResult.should.be.deep.eq({
			RAW: {},
			DISPLAY: {}
		})
	});

	it('should not return any errors if both provider are not available or throws error', async function () {
			let spy = spyService(new FakeProviderThrows(), new FakeProviderThrows())

			let results = await spy.service.getPrices('BTC', 'USD');

			results.should.not.throw
			results.should.have.all.keys('RAW', 'DISPLAY')
		}
	);
});
