import sinon from "sinon";
import CryptoCompareExternalProvider, {httpClient} from "../../../src/core/services/CryptoCompareExternalProvider";

require('chai').should()

describe('Test CryptoCompareExternalProvider', function () {
	it('should fetch data from external service', async function () {
		let mock = sinon.mock(httpClient);

		let response = {
			data: {
				RAW: {},
				DISPLAY: {}
			}
		};

		mock.expects('get')
			.withArgs('/data/pricemultifull')
			.resolves(response)
			.calledOnce

		let result = await CryptoCompareExternalProvider.fetchData('BTC', 'USD')

		result.should.be.deep.eq(response.data)

		mock.verify()
	});
});
