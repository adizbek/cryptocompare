FROM node:16.6.2-alpine3.14 as build

WORKDIR /srv/app

COPY package.json yarn.lock ./

RUN yarn && yarn cache clean

COPY . .

ENV NODE_ENV=production

RUN yarn build

CMD yarn run start:prod
